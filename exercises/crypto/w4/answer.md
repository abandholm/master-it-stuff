% Exercises, week 4: Number Theory - RSA
% Anders Bandholm
% June 10th 2017

Exercise 11.13
==============

Plain RSA is vulnerable to an attack if the same message is sent to multiple recipients. 
The book has an example with 3 recipients -- i.e. 3 keys.

We want to create a security definition that rules out the above attack.

If we were looking at a CPA-secure scheme, the security experiment would allow "oracle access" to get many messages encrypted under the same _key_, but here we have the opposite situation:
We want to see the same _message_ encrypted under different keys.

We will start out with the public key version of "The eavesdropping indistinguishabilty experiment"
$\texttt{PubK}_{A,\Pi}^{\texttt{eav}}(n)$ and modify it a little bit.

The attack presented in the book involves 3 keys, but we will generalize this to $t$ keys.

Given a public-key encryption scheme $\Pi = (\texttt{Gen,Enc,Dec})$ and an adversary $A$,
consider the following experiment:


The eavesdropping indistinguishabilty experiment $\texttt{PubK}_{A,\Pi}^{\texttt{eav-multikey}}(n,t)$
--------------------------

 1. $\texttt{Gen}(1^n)$ is run $t$ times to produce $t$ key-pairs:
 $\left \langle (pk_1,sk_1), (pk_2, sk_2), ... , (pk_t,sk_t) \right \rangle$
 1. Adversary $A$ is given a list of the public keys $\left \langle pk_1, pk_2, ... , pk_t \right \rangle$,
 and outputs a pair of equal length messages $m_0$ and $m_1$ in the message space
 1. A uniform bit $b \in \{0,1\}$ is chosen, and then $t$ ciphertexts
 $\left \langle c_1, c_2, ... , c_t \right \rangle$
 $\leftarrow$
 $\left \langle \texttt{Enc}_{pk_1}(m_b), \texttt{Enc}_{pk_2}(m_b), ..., \texttt{Enc}_{pk_t}(m_b) \right \rangle$
 are computed and given to $A$
 1. $A$ outputs a bit $b'$. The output of the experiment is 1 if if $b' = b$ and 0 otherwise. If $b' = b$ we say that $A$ _succeeds_.


Security definition
-------------------

A public-key encryption scheme $\Pi = (\texttt{Gen,Enc,Dec})$ has _indistinguishable multi-key encryptions in the presence of an eavesdropper_ if for all probabilistic polynomial-time adversaries $A$ there is a negligible function $\texttt{negl}$ such that
$$\forall t \in \mathbb{N} : \Pr[\texttt{PubK}_{A,\Pi}^{\texttt{eav-multikey}}(n,t) = 1] \leq \frac{1}{2} + \texttt{negl}(n))$$


Compared with PubK-CPA-security
-------------------------------

I thought that I could come up with an informal proof that any $\texttt{PubK-CPA}$-secure scheme 
satisfies my definition.

The idea was:

* First I would make the adversary more powerful: I would allow "oracle-access" to ask for the encryption of _any_ $m$
and getting back a list of encryptions of $m$ under the each of the $t$ different keys.
This experiment obviously covers the previous experiment too.
* Next we observe that our experiment is in effect $t$ parallel CPA-experiments

There is one problem though: All $t$ must use the same value of $b$.
And this breaks the idea.

Luckily the need for a proof was removed from the exercise.
