% Exercises, week 5: Discrete Logarithm
% Anders Bandholm
% May 27th 2017

Exercise 11.9
=============


El Gamal Encryption
-------------------

*This is simplified summary leaving out details of how to choose G, g and q -- it is here to summarize the calculations involved.*

A chooses $x$ as (part of her) secret key and calculates $h = g^{x}$ as her public key.

B encrypts message $m$ by choosing a random $y$ and sending
$$(c1, c2) = (g^{y}, h^{y} \cdot m)$$

(Note: $h^{y} = g^{xy}$ - i.e. the shared key in a Diffie-Hellman Key Exchange.)

A can decrypt: $$m := c_{2} \cdot c_{1}^{-x}$$ 
because $$c_{2} \cdot c_{1}^{-x} = h^{y} \cdot m \cdot (g^{y})^{-x} = g^{xy} \cdot m \cdot g^{-yx} = m$$


Chosen Ciphertext Attack
------------------------

We want to prove that El Gamal encryption is not CCA-secure according to definition 11.8 in the book.

In the context of the "IND-CCA" experiment we (the attacker) will choose two cleartexts $m_{0}$ and $m_{1}$.

The Oracle gives us the public key $pk$ and the encryption $(c_{1}, c_{2})$ of
$m$ where $m$ is either $m_{0}$ or $m_{1}$.
Now we must figure out if $m$ is really $m_{0}$ or $m_{1}$.

Since we have access to an decryption Oracle, we can abuse
the malleability of El Gamal and ask for the decryption of
$(c_{1}, 2 \cdot c_{2})$. Let's see what happens:

$$\texttt{Dec}(c_{1}, 2 \cdot c_{2}) =$$
$$2c_{2} \cdot (c_{1}^{x})^{-1} = $$
$$2c_{2} \cdot g^{-xy} = $$
$$2m \cdot g^{xy} \cdot g^{-xy} = $$
$$2m$$

Divide by 2 and we have $m$.

After only one use of the decryption oracle we have found $m$,
and will win this round of the game.
But the technique works in all rounds of the game, so we can in fact win
every round with probability 1 - hence El Gamal is not CCA-secure. $\blacksquare$