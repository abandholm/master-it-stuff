% Exercises, week 1: History and Methodology
% Anders Bandholm
% April 22nd 2017

Exercise 2.6
============

This exercise has a trap built into it, because at first you might think that (a) has perfect security, since the key space is bigger than the message space, and that (b) on the other hand has not not, because here it looks like the key space in one bit smaller that the message space. 

In reality the reverse is true: (a) does not fulfill the requirements for perfect security -- but (b) does.


Justification
-------------


### Scheme (a)

In (a) we see that the key space is larger than the message space, but because the cipher is addition modulo 5, we get in trouble because $0 \equiv 5 \mod 5$. Intuitively the "effective keys" are not 0, 1, 2, 3, 4, 5 but 0, 1, 2, 3, 4, 0 - so if a key is chosen uniformly in the interval [0,5] there will be a skew towards the "effective" key 0.

If the key is 0 or 5 we have 
$$c = m + 0 \mod 5 = m$$
and 
$$c = m + 5 \mod 5 = m$$
which means that the cipher message will be the same as the clear message for 2 out of 6 key values.
For the other key values there will be a one to one mapping between key and cipher text.

More formally: If we observe any cipher text $c'$, the most likely value for $m$ is $c'$:

$$ Pr[ m = c' | c = c' ] = \frac{2}{6} $$
but without any observed cipher text
$$ Pr[ m = m' ] = \frac{1}{5} $$
Which means that
$$Pr[ m = m' ] \neq Pr[ m = m' | c = c' ]$$
and hence we do not have pefect security.

So after observing any cipher message we _do_ have information on the clear message - and for perfect secrecy this should not be the case.


### Scheme (b)

The size of the message space $|M|$ _seems_ to be larger than the key space, but if an $l$-bit bit-string always ends with 0,
it does not represent $2^l$ different values but only $2^{l-1}$, so the actual sizes are
$$ |M| = |K| = 2^{l-1} $$
so the key space _is_ big enough to satisfy one of the requirements for perfect secrecy.

The scheme has perfect secrecy because it is actually an $n$-bit one time pad (where $n=l-1$) with a zero added 
to both messages and cipher texts.
