% Exercises, week 2: Building blocks of symmetric-key cryptography
% Anders Bandholm
% April 30th 2017

Exercise 6.16
=============

DESY is defined as $$DESY_{k,k'}(m) :=  DES_{k}(m \oplus k')$$
Our task is to design a key recovery attack.
According to the errata: "there is in fact an attack taking time $2^{56}$ and using only constant space."


Our attack
----------

Assume we have one pair $(m,c)$ where $c = DES_{k}(m \oplus k')$

To find $k$ and $k'$ we observe that if we could somehow _guess_ $k$, then we can calculate $k'$:

$$c = DES_{k}(m \oplus k') \Leftrightarrow k' = DES^{-1}_{k}(c) \oplus m$$

So if we do an exhaustive key search on $k$, for each tried $k$ we can calculate a corresponding $k'$.
This construction will work for _any_ key $k$ that we try, so we cannot learn if our guess is correct:
With only one pair $(m,c)$ we cannot recover the key(s).


### Two pairs of $m$ and $c$

Now suppose we have _one more_ pair $(m',c')$ encrypted with the same keys as above.

We can now use the second pair to verify the keys -- i.e. we can construct this algorithm:

    foreach k in 0..2^56
        k' := InvDES(k, c) xor m;
        if DESY(k, k', m') == c' output k, k';

This will run in $2^{56}$ rounds - but will actually use 2 calls of DES or InvDES in each round so
I suppose the time is not $2^{56}$ but $2^{57}$. 
This is still better than $\approx 2^{120}$ that a brute forces attack will require.

Just like the "meet in the middle" attack, adding a more pairs to the test will make the 
likelihood of a false positive, arbitrarily small.