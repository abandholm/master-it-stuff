% Exercises, week 3: Symmetric Cryptography
% Anders Bandholm
% May 21st 2017

Exercise 3.19
=============


Scheme (a)
----------

The attacker can compute $G(r)$, since both the generator $G$ and the random $r$ is known to the attacker.
The message $m$ is then easily recovered.
In the "game context" the attacker does not need to guess, he can simply calculate $m$, hence
this scheme is not EAV-secure, and obviously not CPA-secure either.


Scheme (b)
----------

Since $F_{k}(0^{n})$ is a constant, $m \oplus F_{k}(0^{n})$ is definitely not CPA-secure: 
An attacker can use the oracle to encrypt $0^{n}$ to learn the constant, 
and after that easily recover which of $m_{0}$ and $m_{1}$ that was encrypted.

Is it EAV-secure? Yes. It is in fact equivalent to the One Time Pad of length $n$ 
because $F$ is a _pseudorandom function_ its output is indistinguishable from a real random bitstring.

And just like the one time pad it is secure until you reuse the key stream -- then it fails.

Scheme (c)
----------

This scheme is CPA-secure.

Intuitively: This scheme is similar to "Construction 3.30" in the book, but the added "noise"
seems smaller, and might endanger the CPA-security.

In the proof of Theorem 3.31 we look at the risk of the Oracle choosing the same $r$ again.
If that happens, the attacker will -- in this round of the game -- know the value of $F_{k}(r)$,
and can the easily distinguish between $m_{0}$ and $m_{1}$.
To break the scheme the attacker can use the Oracle again and again until $r$ repeats.
The size of $r$ determines if this attack is feasible or not.

In broad terms Theorem 3.31 shows us that because of the length of $r$ the risk of a repeated $r$ is negligible.

But what about scheme (c) then?

The risk of the Oracle outputting a "revealing r" is doubled because the attacker can hope for either $r$ or $r+1$
to match $r^{*}$.
If $r$ matches $r^{*}$ he can decrypt the first half and if $r+1$ matches $r^{*}$ he can decrypt the second part.
In both cases he will know if he is looking at $m_{0}$ or $m_{1}$, and win the game.

Is it still CPA-secure then? Yes, because the crucial part of the proof of 3.31 is that $\frac{q(n)}{2^{n}}$
is a negligible function (the exponential function will always outgrow the polynomial).
In this case we have doubled the attackers chance of hitting a revealing $r$, and the formula is instead
$$\frac{q(n)}{2^{n-1}}$$
but this is still negligible.
