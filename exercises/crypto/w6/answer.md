% Exercises, week 6: Hash Functions and Digital Signatures
% Anders Bandholm
% June 8th 2017

\newcommand{\hs}[1]{H^{s}(#1)}
\newcommand{\hhs}[1]{\widehat{H}^{s}(#1)}


Exercise 5.3
=============

Let $(\texttt{Gen}, H)$ be a collision-resistant hash function.
And define $(\texttt{Gen}, \widehat{H})$ this way:

$$\hhs{x} := \hs{\hs{x}}$$

We want to find out if this definition will also make $(\texttt{Gen}, \widehat{H})$ collision-resistant.


Analysis
--------

Assume we have found a collision in $\hhs{}$ -- i.e:

$$ \hs{\hs{x}} = \hs{\hs{'x}}, \;  \text{where} \; x \neq x' $$

To analyze this we look at the illustration below, where we follow the path of
$x$ and $x'$
through the two iterations of $\hs{}$

$$
\begin{matrix}
\text{input} &             & \text{step 1} &             & \text{step 2} \\
    x        & \rightarrow & H(x)          & \rightarrow & H(H(x)) \\ 
    \neq     &             & ?             &             & = \\ 
    x'       & \rightarrow & H(x')         & \rightarrow & H(H(x'))
\end{matrix}
$$

where $H(x)$ is short for $\hs{x}$.

Since we have a collision in $\hhs{}$, we know the two inputs on the left are different, 
and the outputs on the right are the same.
We do not know if the intermediate values after step 1 are the same (hence the question mark)

The collision in the right column (step 2) can arise in two different ways:

1. The inputs to step 2 are the same - which means there must be a collision in step _one_
1. The inputs to step 2 are different - which means there must be a collision in step _two_

So if we find a collision in $\hhs$ it means that we have also found a collision in $\hs$,
but since $\hs$ is collision-resistant this is hard: 
The probabilty of winning the "collision game" is _negligible_ - i.e.

$$Pr[\texttt{Hash-coll}_{\emph{A},\prod}(n) = 1] \leq \texttt{negl}(n)$$

So it seems that in the worst case the construction of $\hhs$ doubles the likelihood of finding a collision in $\hhs{}$
compared to $\hs{}$ alone.

But $2 \cdot \texttt{negl}(n)$ is still negligible, so we conclude that 
$(\texttt{Gen}, \widehat{H})$
is also collision-resistant.