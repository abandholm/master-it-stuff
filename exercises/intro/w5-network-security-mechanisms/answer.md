% Exercises, week 5: Network Security Mechanisms
% Anders Bandholm
% Sunday March 12th, 2017

Exercise 4.2
============

The exercise talks about a URL in the certificate.
In certs for web-servers it is normally just the hostname that is stored.

All 3 questions deal with checking or not checking that the URL/hostname
is correct.


Question 1
----------

> Argue that this check makes a difference to security: describe
> one or more security problems that may occur if this check is not
> performed.  The threat model includes attacks where an adversary
> can hijack http connections, i.e. insert/remove messages of his
> choice, or completely take over a connection.

It is absolutely essential to check that the hostname inside the certificate matches the hostname you intended to communicate with. If you do not check you don't really know who you are talking to.

### Example attack

The attacker tricks your browser into connecting to his own server instead of the intended one.
One way for the attacker to do this is to manipulate DNS using a [DNS cache poisoning](https://en.wikipedia.org/wiki/DNS_spoofing#Cache_poisoning_attacks) attack so that your DNS query will return the attackers IP instead of the correct one.

When you connect to his IP instead of the correct one, your browser will assume that is connected to the right address.
The attackers web site will now send his _own_ certificate.
It does not matter which certificate as long as it is valid and the attacker has the corresponding private key.

At this point the browser can check the hostname inside the cert and discover the deception, but if it does not check it will not discover the attack.


Question 2
----------

> If a URL is not present in the certificate, (or if you do not
> want to require that the URLs match), the check we looked
> at can of course not be performed. What could the browser
> do instead to solve the problems you identified above?

### Purpose of certificates

The whole purpose of a certificate is to prove that a given public key belongs to a certain id.
If the cert does not contain an id, there is actually not much point in using it![^id] But let's play along...

[^id]: Philosophical note: What is an id? In _cryptography_ an _id_ is typically just a key, and in the _network_ an id is an IP-number (or a hostname). If you want to connect to a "network-id" the cert needs to link these two ids. Or in other words: A certificate needs to contain the appropriate id for the context: In a network-connection context this is the hostname or IP.

If the browser can not verify the id (in this case the hostname), the connection is basically _unauthenticated_.


### Adding another authentication protocol

One way to fix the problem is to use another authentication protocol on top of the SSL/TLS protocol. For the "one-sided" version the client could send a nonce $n_{0}$ and the server could authenticate by sending $MAC(n_{0}, K)$ where _K_ is pre-shared key.
It is not super practical since it requires a pre-shared key, and a good AKE will allow you to communicate securely _without_ previous contact.

### SSH-like solution

Another solution could be to use fixed certificates: The user is asked to accept the server-cert, and in all future connections the browser accepts just that one cert.
If another cert is sent by the server, the user needs to inspect it and (maybe) accept it as the new "static" cert.

### Example: WPA2

WPA2 with PEAP and MSCHAPv2 is -- at least in some implementations -- an example of both of the above ideas:

 * The certificate is basically used to create an encrypted
   tunnel, while the real (and mutual) authentication is done
   with MSCHAPv2.
 * Some clients asks the user to confirm the certificate and
   ignores the CA and hostname (this allows e.g. a telephone
   to use so-called "WPA2 Enterprise" where an internal CA is 
   used, but the phone does not trust this CA out of the box)

### Trusting the CA

If we _do_ check the id in the cert we now rely on the CA to have done proper checking that the given hostname really belongs to whomever is trying to buy a certificate.
Sadly some CAs have historically done a bad job, but in _most_ cases you can trust the id in the cert.
I think bad CAs is one of the reasons that some people prefer to use other means to prove ownership of keys -- like the SSH-model described above.


Question 3
----------

> The handshake and other protocols in SSL can be proved secure
> using a number of methods (although the exact definition of
> security used is not always the same). Yet we have now seen
> some security problems occurring if the check on the URL
> (or some other check) is not done, despite the fact that no
> such verification is required in the SSL specification. This
> question is designed to give an explanation for this weird
> phenomenon.
> 
> In the notes, we gave a definition an AKE, which we will call
> $D$ here.  This definition requires that each party starts
> the protocol with an input that specifies the identity of
> the other party you want to exchange a key with. One could
> also consider an alternative definition $D'$ where there is no
> input, but if the protocol is successful, the output is a key
> plus the identity of the party you have managed to exchange
> a key with. In $D'$ , the Agreement and Authentication/Secrecy
> requirements would say
> 
> * If $A$ outputs ($K_{A}$ , identity $B$) and $B$ outputs
>   ($K_{B}, identity $A$), then $K_{A} = K_{B}$.
> * if $A$ outputs ($K$, identity $B$), then $B$ participated 
>   in the protocol, and he must output either ($K$, identity
>   $A$), or “reject”. Furthermore, the adversary does not 
>   know $K$.
> 
> Suppose you are given an AKE protocol $\pi$ that satisfies
> definition $D'$.  Show how to construct from $\pi$ a new AKE
> protocol that satisfies definition $D$. You are looking for
> a very simple solution in which your protocol may fail more
> often than $\pi$.  
> 
> Which of the two definitions $D$, $D'$ does the
> SSL AKE satisfy?


Bottom line: It looks like something important was forgotten in SSL, but if we add the check on the hostname we have have an AKE that satisfies $D$.

We construct a protocol like this:

 1. _A_ intends to talk to _B_, and _B_ intends to talk to _A_
 1. _A_ and _B_ perform $\pi$
 1. If $\pi$ fails we abort the protocol
 1. If $\pi$ succeeds, _A_ and _B_ must check that the identities match:
    * _A_ must check that the identity for _B_ that is output by $\pi$ is indeed the same id that she wanted to connect to -- if not she rejects
    * similarly _B_ checks _A_'s identity and rejects on mismatch

This protocol satisfies $D$, but will fail more often than $\pi$: It will fail exactly if the ids do not match.

The two-sided version of SSL/TLS (with client certs) satisfies $D'$, but if the id check was made mandatory, it does in fact satisfy $D$.[^SNI]

[^SNI]: In modern TLS there is an extension called SNI: Server Name Indication, in which the client can indicate the hostname that he intends to talk to. This is really for another purpose: To let the server choose the right certificate to present, when several "virtual" servers share a single IP-number. But in principle it could be used in the protocol so the verification of the hostname could be done _inside_ the protocol.
