#!/usr/bin/env perl 
#===============================================================================
#
#         FILE: rsa.pl
#
#        USAGE: ./rsa.pl  
#
#  DESCRIPTION: Demonstrate RSA-functions
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: YOUR NAME (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 2017-02-05 15:45:38
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;

use Math::BigInt;
use POSIX qw/INT_MAX/;

# this function was borrowed here: https://en.wikibooks.org/wiki/Algorithm_Implementation/Mathematics/Primality_Testing
# since Perl's Bigint does not contain one
#
sub is_probable_prime($$){
  my ($n,$k) = (Math::BigInt->new($_[0])->babs(),$_[1]);
  return 1 if $n == 2 || $n == 3;
  return 0 if $n < 5 || ($n & 1) == 0;
  my ($s,$d) = (0,$n-1);
  ($s,$d) = ($s+1,$d>>1) while ($d & 1) == 0;
  my $maxa = ($n-1 < int(INT_MAX) ? $n-1 : int(INT_MAX));
  for my $i (0..$k){
    my $a = Math::BigInt->new(int(rand($maxa-2)+2));
    my $x = $a->bmodpow($d,$n);
    if($x != 1 && $x+1 != $n){
      for my $j (1..$s){
        $x = $x->bmodpow(2,$n);
        return 0 if $x == 1;
        if ($x == $n - 1){
          $a = 0;
          last;
        }
      }
      return 0 if $a;
    }
  }
  return 1;
}


sub random_bits($)
{
	my ($length) = @_;

	my $bytes = $length >> 3;

	my $F;
	open($F, '<', '/dev/urandom') or die;
	my $binary;
	read($F, $binary, $bytes);
	close;

	my $hex = unpack('h*', $binary);

	print STDERR "DEBUG: hex: $hex\n";
	return Math::BigInt->new('0x' . $hex);
}

sub gen_probable_prime($)
{
	my ($length) = @_;

	my $prime_size = sqrt($k);
	my $random = random_bits($prime_size);
}

my $k = 10;

print STDERR random_bits(64), "\n";
