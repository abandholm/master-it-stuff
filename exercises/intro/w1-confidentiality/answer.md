% Exercises, week 1: Confidentiality
% Anders Bandholm
% Saturday February 4th, 2017

Exercise 1.1
============

First bullet
------------

The value of bit 20 represents $2^{20} = 1048576$ and will be one only in numbers higher than or equal to 1048576. Since the hacker knows that his salary is less than a million, the value of bit 20 must be zero.

If the hacker flips the 20th bit _of the ciphertext_ he will increase his payment with 1048576 kr!

This happens because the key in a one-time pad is xor'ed to the cleartext, so that flipping a bit in the ciphertext will flip the corresponding bit in the decrypted text.

Assuming that $m = 0$ we have:

$$c = 0 \oplus k = k$$

where _m, c_ and _k_ refer to the 20th bit only.

Let $c'$ denote the flipped version of $c$:

$$c' = inv(c) = 1 \oplus c$$

since xor'ing with one effectively flips the input bit. Let's calculate the modified m that results from this -- remembering that $c = k$

\begin{align}
m' &= c' \oplus k \\
   &= 1 \oplus c \oplus k \\
   &= 1 \oplus k \oplus k \\
   &= 1 
\end{align}

So flipping bit 20 in the ciphertext actually changes bit 20 of the decrypted message to _one_ (still under the assumption the the original cleartext bit was 0)



Second bullet
-------------

The problem that we have seen in bullet one is an _authenticity problem_ since by definition authenticity deals with the ability to _modify_ the message. And clearly the hacker is able to do that. In fact he can change any bit he wants.

It is _not_ confidentiality problem because the attacker did not learn anything new: He already knew the value of one cleartext bit, but did not learn the value of any other bits.



Third bullet
------------

Does the above suggest that the one-time pad is broken? 

No: If the attacker knows a number of cleartext bits he can calculate the corresponding key bits, but since the key bits are _truly random_ knowing part of the key reveals nothing about the rest of the key, and hence no new cleartext bits either.

Nonetheless it is poor use of the one-time pad, but the problem is in the _protocol_. It is easy to fix: If you are prepared to send a longer message, one idea could be xor'ing a 21 bit random nonce to the input and prepending the nonce to the message before encryption.


Fourth bullet
-------------

This is in a way a generalization of the problem above. The attack above was possible because we assumed that the attacker _knew_ bit 20. But in reality his income could have increased (without him knowing it) so that bit 20 was already 1  - but the likelihood is of course very low.

Back to _this_ exercise: 
If we first assume that $p$ is high -- or more precisely $p > 0.5$ then the attacker will not gain anything by flipping the bit since in most cases the bit will be 0 already. 
If on the other hand $p$ is low ($p < 0.5$) it means the in most cases the bit will be a one, so in this case he will flip the bit and now the bit is 0 with probability $1-p$.
If $p = 0.5$ he will have no idea if the bit is one or zero and cannot improve that by changing it.

What is the probability that the attacker can achieve the goal and the receiver receives a 0?

$$
p_{receive 0} = 
  \begin{cases} 
   p     & \text{if } p \geq 0.5 \\
   1-p   & \text{if } p < 0.5
  \end{cases}
$$

Since the attacker can choose the strategy for bit-flipping based on $p$ and will choose to flip in exactly those cases where $1-p$ is bigger than $p$ and not to flip when $p$ is bigger than $1-p$ -- in other words choosing based on the maximum of $p$ and $1-p$ -- i.e. $p_{receive 0} = max(p, 1-p)$.