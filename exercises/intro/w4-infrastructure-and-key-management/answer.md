% Exercises, week 4: Infrastructure and Key Management
% Anders Bandholm
% Friday March 3rd, 2017

Exercise 3.1
================================

Question 1
----------

If the communication is performed in the clear, the UNIX-like system breaks immediately: Any eavesdropper will learn the credentials and can abuse them. The other scheme (with public keys), is not weakened by clear text communication since a passive attacker will not gain any insight in the private key of the user, and therefore cannot impersonate him.

If we use encrypted communication (e.g. SSL/TLS in the form of HTTPS^[Exchanging passwords "in clear", but over HTTPS, is the most common real-world method to handle logins to websites. Both form-based login and HTTP's "Basic Auth"]) both systems are secure since in this case the passive attacker will, not get any information about the credentials exchanged.



Question 2
----------

Now the adversary is inside the system and can read -- but not modify -- the files.

When the attacker is inside, he can steal the password-file, and do an offline attack: Try to hash a lot of different texts to see if the result matches one of the hashes in the file. If the system has many users, this is a very effective attack that will most likely find several passwords.

The public-key-version holds up better, since public keys are *designed* to be public, and the attacker will not be able to find the corresponding private keys necessary for an impersonation.

But if the attacker is able to *change* things both schemes are vulnerable: The attacker can replace the password of any user with a hashed version of a known password, and then log in as that user. He can even reinstall the original hashed password so the user will not discover the breach.

And the public-key scheme is no better: The attacker can replace the keys with his own, login as the user, and replace the original key afterwards.


Question 3
----------

It seems from the comparisons above that public-keys have an advantage over hashed passwords -- but maybe a smaller advantage than you might have expected.

You can actually improve the hashed-password scheme by introducing nonces, and then you can do a handshake -- even in an unencrypted channel -- that does not reveal the password (e.g. [MSCHAPv2](https://tools.ietf.org/html/rfc2759) that is used inside many popular WPA2-solutions)

To make the public key-scheme more safe against modification on file you could replace them with full certificates. But certificates introduce an administrative overhead (renewal!) that many prefer to avoid.

### If public-key crypto was required

The solution in the exercise is a "roll your own" solution (generally a bad idea!) but here the problem is mainly that standard software (browsers, servers, etc.) do not support this out of the box. You may have to write plugins for clients and servers.

I would prefer a more elaborate system, probably in the form of X.509-certs on the server, and the private keys for users kept in smart-cards where all client-side crypto takes place. To avoid stolen pass phrases I would prefer a smart-card with a physical keyboard for PIN-entry.

The server could have its own CA with the private key residing in a special hardware module.

One step down in security would be to use client-certs protected by normal pass phrases, but in this case the solution _is_ supported by normal web browsers and servers.


### Preferred solution

Today -- for most real world systems -- I think I prefer the hashed-password over the public-key-scheme. Mostly because the private key is a bit problematic to handle in many situations: 

* You can login only from devices that hold your private key, 
* or you need special hardware (smartcard-reader)
* the key is vulnerable to a keylogger attack if stored in an encrypted file on disk.

A password on the other hand is -- if used correctly -- stored only in your brain and can be brought with you everywhere.

I would use a one-way-function such as _bcrypt_ or _scrypt_ that use _salt_ and where the work factor can be increased over time.

If I want stronger auth than this, I would add two-factor-auth before going to a public key solution. My favorite is the Yubikey, that requires only a USB-port.

With this solution I am in line with most recommendations for today's web, but I find it likely that we will have even better solutions in the future that will be based on public key crypto.