% Exercises, week 3: Authentication
% Anders Bandholm
% Sunday March 5th, 2017

Exercise 2.1 - corrected version
================================

In my first submssion I was so focused on an attack on the privacy, that I didn't look carefully enough on the authenticity part of the problem, and boldly wrote that no problems existed with authenticity. Oops.

*This version is updated with respect to the authenticity. If I got it right this time, this is enough to show that the first suggestion breaks the security policy. But because I like the separate attack on the privacy, this attack is still here.*

We are studying these two suggestions to see if they can be used to satisfy a security policy:

$$E_{pk_{D}}(R),S_{sk_{A}}(E_{pk_{D}}(R)), A$$

$$E_{pk_{D}}(R, S_{sk_{A}}(R)), A$$

The first part of the security policy is about the identity of the sender:

> When a request for information arrives, D should be able to
> determine which user sent the request.

First we note that both suggestions include a signature based on $A$'s secret key, but unfortunately this is not enough: In the first suggestion it is possible for an adversary ($E$) to "steal" the encrypted request by removing the original signature and replacing it with her own:

$$E_{pk_{D}}(R),S_{sk_{A}}(E_{pk_{D}}(R)), A$$
$$\rightarrow$$
$$E_{pk_{D}}(R),S_{sk_{E}}(E_{pk_{D}}(R)), E$$

The database $D$ will not be able to determine if $A$ or $E$ sent the request and hence the security policy is violated[^1]

The other suggestion does not have this problem since the signature is protected by the encryption.


Direct attack on the privacy of the request
-------------------------------------------

Looking at the **first suggestion** again:

$$E_{pk_{D}}(R),S_{sk_{A}}(E_{pk_{D}}(R)), A$$

we note that an attacker can get information that really should not be available to him:

1. if $A$ sends the same request $R$ twice, $E_{pk_{D}}(R)$ will be the same and the attacker will know that the request must have been the same. This is a problem, but it does not violate the security policy.

1. since the attacker knows $pk_{D}$, he can actually try out different guesses $R'$, and compute $E_{pk_{D}}(R')$ until he finds an $R'$ where $E_{pk_{D}}(R')$ matches $E_{pk_{D}}(R)$, which means that he has found $R$. This is clearly a violation of the second bullet in the security policy.

Since we are talking about a database-request and not random binary data, the attack is probably quite realistic.

The **second suggestion:**

$$E_{pk_{D}}(R, S_{sk_{A}}(R)), A$$

works better:

1. if $A$ sends the same request twice the attacker will again notice, because all parts that are being encrypted are the same. As before it is a problem, but not a violation of the policy.

1. since the attacker does not have the secret key of $A$, he can not figure out what $S_{sk_{A}}(R)$ is -- even knowing $R$ -- so he will not be able to try guesses at R and figure out the original request.

So in this case the security policy _is_ satisfied.


What we can learn from this
---------------------------

First we see here a "proof" of the the fact that it is difficult to combine authenticity and privacy without making mistakes...

You can also argue that what is missing in both suggestions is some "noise" added to the message before encryption. In fact this is also true for the _answer_, since $E_{pk_{A}}(data)$ will also be the same when $data$ is the same.

So both suggestions will improve if random noise is added to the arguments before encryption. I would suggest using random
padding like OAEP, but other sources of noise might also work.

And then the security policy should be updated to add an extra point:

 * A user should not be able to get information about the _results_ that $D$ sends to other users.

It might also be a good idea to move the identity ('A') inside  the encryption:

$$E_{pk_{D}}(R, S_{sk_{A}}(R), A)$$

This will make the identity of A unknown to an attacker[^2]. This protection could also be added to the security policy.

[^1]: At first this may seem to be a small problem in this context since $E$ cannot use this to elevate her privileges to those of $A$, but suppose that the database responses contain both the query _and_ the response -- then $E$ will get to know both the request and the result. In other contexts the problem will be more obvious.

[^2]: In real life this is not easy because A can be identified in a lot of other ways like IP-address, etc.
